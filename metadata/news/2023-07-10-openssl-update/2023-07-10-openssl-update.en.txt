Title: openssl 3 update
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2023-07-10
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-libs/openssl:0

openssl 3.x has been unmasked and 1.1.1x will hit its end-of-life on
2023-09-11. Due to security implications you're advised to upgrade in
time and remove the old version from your system.

Reinstall all dependents:
# cave resolve --reinstall-dependents-of openssl:0 nothing

systemd might fail to rebuild and can be worked around by reinstalling
util-linux first:
# cave resolve util-linux -x1

Once finished rebuilding all dependents you are advised to uninstall
openssl:0 from your system. Note that there might still exist packages
depending on openssl:0 which will be listed for you to manually check
and decide about further actions:
# cave uninstall openssl:0 -x

As a final step be sure to run cave fix-linkage to make sure nothing is
left in a broken state:
# cave fix-linkage -x1
