# Copyright 2008 Ciaran McCreesh
# Copyright 2009 Mike Kelly
# Copyright 2013 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN}-py project=${PN}-py release=${PV} suffix=tar.gz ]
require setup-py [ import=setuptools blacklist='2' has_bin=true test=pytest ]

SUMMARY="Text based document generation"
DESCRIPTION="
AsciiDoc is a text document format for writing notes, documentation, articles,
books, ebooks, slideshows, web pages, man pages and blogs. AsciiDoc files can be
translated to many formats including HTML, PDF, EPUB, man page.  AsciiDoc is
highly configurable: both the AsciiDoc source file syntax and the backend output
markups (which can be almost any type of SGML/XML markup) can be customized and
extended by the user.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        app-text/docbook-xml-dtd:4.5
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
    test:
        dev-python/pytest-mock[python_abis:*(-)?]
    suggestion:
        app-text/docbook-dsssl-stylesheets
"

UPSTREAM_DOCUMENTATION="
    https://powerman.name/doc/asciidoc   [[ description = [ Cheatsheet ] ]]
    https://asciidoc.org/userguide.html  [[ description = [ User Guide ] ]]
    https://asciidoc.org/faq.html        [[ description = [ FAQ ] ]]
"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/blob/${PV}/CHANGELOG.txt"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-10.2.0-testasciidoc-path.patch
    "${FILES}"/${PN}-10.2.0-testasciidoc-usage.patch
    "${FILES}"/${PN}-10.2.0-recognize-etc-asciidoc.patch
)

src_install() {
    setup-py_src_install

    # Manually install resources in shared directories
    edo cd "${WORKBASE}"/PYTHON_ABIS/${PYTHON_ABIS##* }/${PNV}
    insinto /usr/share/${PN}
    doins -r asciidoc/resources/{icons,javascripts}

    insinto /etc/${PN}
    doins -r asciidoc/resources/!(icons|javascripts)

    dosym /usr/share/${PN}/javascripts /etc/${PN}/javascripts
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # do not install stuff into site-packages
    edo rm -rf "${IMAGE}"$(python_get_libdir)/site-packages/${PN}/resources
    # remove generated pycache from etc
    edo rm -rf "${IMAGE}"/etc/${PN}/filters/{,{code,graphviz,latex,music}}/__pycache__

    doman doc/{a2x,${PN}}.1
}

pkg_preinst() {
    if has_version 'app-doc/asciidoc[<10.2.0]' ; then
        # /etc/asciidoc/javascripts is now a symlink instead of a directory
        [[ -d "${ROOT}"/etc/asciidoc/javascripts ]] && edo rm -r "${ROOT}"/etc/asciidoc/javascripts
    fi
}

