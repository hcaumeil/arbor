# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Copyright 2015-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'doxygen-1.5.5.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require cmake utf8-locale
require option-renames [ renames=[ 'qt5 doxywizard' ] ]

SUMMARY="Documentation system for C++, C, Java, Objective-C, Python, IDL, and other languages"
HOMEPAGE="https://www.doxygen.nl"
DOWNLOADS="${HOMEPAGE}/files/${PNV}.src.tar.gz
    doc? ( ${HOMEPAGE}/files/${PN}_manual-${PV}.pdf.zip )"

UPSTREAM_DOCUMENTATION="
${HOMEPAGE}/manual/faq.html [[ description = [ Doxygen FAQ ] lang = en ]]
${HOMEPAGE}/manual/index.html [[ description = [ Doxygen Manual ] lang = en ]]
"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/manual/changelog.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    dot      [[ description = [ Use the dot tool from graphviz to generate more advanced diagrams
                                and graphs ] ]]
    doxywizard [[ description = [ Build a GUI tool to configure and run doxygen ] ]]
    examples [[ description = [ Build doxyapp ] ]]

    doxywizard? (
        ( providers: qt5 qt6 ) [[
            number-selected = exactly-one
        ]]
    )
"

# tests require latex (bibtex)
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/python:*
        dev-scm/git
        sys-devel/bison[>=2.7]
        sys-devel/flex[>=2.5.37]
        doc? ( virtual/unzip )
    build+run:
        app-text/ghostscript
        dev-db/sqlite:3
        dev-libs/fmt
        dev-libs/spdlog
        doxywizard? (
            providers:qt5? ( x11-libs/qtbase:5[gui] )
            providers:qt6? ( x11-libs/qtbase:6 )
        )
    run:
        dot? (
            media-gfx/graphviz[>=1.8.10]
            media-libs/freetype:2
        )
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( LANGUAGE.HOWTO )

src_configure() {
    local cmake_params=(
        -Dbuild_doc:BOOL=FALSE
        -Dbuild_doc_chm:BOOL=FALSE
        -Dbuild_parse:BOOL=FALSE
        -Dbuild_search:BOOL=FALSE
        # Unpackaged (https://github.com/javacc/javacc), would recreate some files
        # for vhdlparser, but falls back to the ones shipped with the tarball.
        -DCMAKE_DISABLE_FIND_PACKAGE_Javacc:BOOL=FALSE
        # https://pypi.org/project/generateDS/, automatically recreates the XML
        # parser modules when updating the schema files
        -DCMAKE_DISABLE_FIND_PACKAGE_generateDS:BOOL=FALSE
        -DENABLE_CLANG_TIDY:BOOL=FALSE
        -Denable_coverage:BOOL=FALSE
        -DSANITIZE_ADDRESS:BOOL=FALSE
        -DSANITIZE_MEMORY:BOOL=FALSE
        -DSANITIZE_THREAD:BOOL=FALSE
        -DSANITIZE_UNDEFINED:BOOL=FALSE
        -Duse_libclang:BOOL=FALSE
        # Only set if compiler is clang, however I can't easily test this
        -Duse_libc++:BOOL=FALSE
        -Duse_sys_spdlog:BOOL=TRUE
        -Duse_sys_sqlite3:BOOL=TRUE
        $(cmake_option examples build_app)
        $(cmake_option examples build_xmlparser)
        $(cmake_option doxywizard build_wizard)
    )

    if option providers:qt5 ; then
        cmake_params+=( -Dforce_qt=Qt5 )
    elif option providers:qt6 ; then
        cmake_params+=( -Dforce_qt=Qt6 )
    fi

    ecmake "${cmake_params[@]}"
}

src_test() {
    # UnicodeDecodeError on Python 3, last checked: 1.8.14
    require_utf8_locale

    cmake_src_test
}

src_install() {
    cmake_src_install

    if option doc; then
        insinto /usr/share/doc/${PNVR}
        doins "${WORKBASE}"/${PN}_manual-${PV}.pdf
    fi
}

