# Copyright 2008, 2009, 2011, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'sqlite-3.5.6.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require flag-o-matic

SCHNARF_PV="$(ever range 1)$(printf "%02d" $(ever range 2))\
$(printf "%02d" $(ever range 3))\
$(printf "%02d" $(ever range 4))"

SCHNARF_PV_DOC="$(ever range 1)$(printf "%02d" $(ever range 2))$(printf "%02d" $(ever range 3))00"

SUMMARY="An embedded SQL database engine"
HOMEPAGE="https://www.sqlite.org"
DOWNLOADS="${HOMEPAGE}/2025/${PN}-src-${SCHNARF_PV}.zip
    ${HOMEPAGE}/2025/${PN}-autoconf-${SCHNARF_PV}.tar.gz
    doc? ( ${HOMEPAGE}/2025/${PN}-doc-${SCHNARF_PV_DOC}.zip )"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/releaselog/$(ever replace_all _).html [[ lang = en ]]"

LICENCES="public-domain"
SLOT="3"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    debug
    doc
    readline
    static [[ description = [ Build static version of libsqlite3 ] ]]
"

DEPENDENCIES="
    build:
        virtual/unzip
    build+run:
        sys-libs/zlib
        readline? ( sys-libs/readline:= )
    test:
        dev-lang/tcl[>=8.6]
"

# The autoconf tarball includes the amalgamated source, so it doesn't require tcl
# at build time, but it does not include the tests.
WORK=${WORKBASE}/${PN}-autoconf-${SCHNARF_PV}/

sqlite_src_build() {
    # Switch to using the "src" zip if tests are enabled, but only when not cross-compiling,
    # since the tcl dependency causes issues.
    expecting_tests && exhost --is-native
}

pkg_setup() {
    sqlite_src_build && WORK=${WORKBASE}/${PN}-src-${SCHNARF_PV}/
}


SQLITE_OPTIONS=(
    # Support detection of misuse of SQLite API.
    # https://sqlite.org/compile.html#enable_api_armor
    -DSQLITE_ENABLE_API_ARMOR

    # Support bytecode and tables_used virtual tables.
    # https://sqlite.org/compile.html#enable_bytecode_vtab
    # https://sqlite.org/bytecodevtab.html
    -DSQLITE_ENABLE_BYTECODE_VTAB

    # Support column metadata functions.
    # https://sqlite.org/c3ref/column_database_name.html
    -DSQLITE_ENABLE_COLUMN_METADATA

    # Support sqlite_dbpage virtual table.
    # https://sqlite.org/compile.html#enable_dbpage_vtab
    -DSQLITE_ENABLE_DBPAGE_VTAB

    # Support dbstat virtual table.
    # https://sqlite.org/dbstat.html
    -DSQLITE_ENABLE_DBSTAT_VTAB

    # Support comments in output of EXPLAIN.
    # https://sqlite.org/compile.html#enable_explain_comments
    -DSQLITE_ENABLE_EXPLAIN_COMMENTS

    # Enable the FTS3 enhanced query syntax
    # https://sqlite.org/fts3.html#compiling_and_enabling_fts3_and_fts4
    -DSQLITE_ENABLE_FTS3_PARENTHESIS

    # Support hidden columns.
    -DSQLITE_ENABLE_HIDDEN_COLUMNS

    # Support sqlite3_normalized_sql() function.
    # https://sqlite.org/compile.html#enable_normalize
    # https://sqlite.org/c3ref/expanded_sql.html
    -DSQLITE_ENABLE_NORMALIZE

    # Support sqlite_offset() function.
    # https://sqlite.org/lang_corefunc.html#sqlite_offset
    -DSQLITE_ENABLE_OFFSET_SQL_FUNC

    # Support Resumable Bulk Update extension.
    # https://sqlite.org/rbu.html
    -DSQLITE_ENABLE_RBU

    # Support scan status functions.
    # https://sqlite.org/c3ref/stmt_scanstatus.html
    # https://sqlite.org/c3ref/stmt_scanstatus_reset.html
    -DSQLITE_ENABLE_STMT_SCANSTATUS

    # Support sqlite_stmt virtual table.
    # https://sqlite.org/stmt.html
    -DSQLITE_ENABLE_STMTVTAB

    # Support unknown() function.
    # https://sqlite.org/compile.html#enable_unknown_sql_function
    -DSQLITE_ENABLE_UNKNOWN_SQL_FUNCTION

    # Support unlock notification.
    # https://sqlite.org/unlock_notify.html
    -DSQLITE_ENABLE_UNLOCK_NOTIFY

    # Support soundex() function.
    # https://sqlite.org/lang_corefunc.html#soundex
    -DSQLITE_SOUNDEX

    # Support URI filenames.
    # https://sqlite.org/uri.html
    -DSQLITE_USE_URI

    # Save a little CPU time on LIKE queries
    # https://www.sqlite.org/compile.html#like_doesnt_match_blobs
    -DSQLITE_LIKE_DOESNT_MATCH_BLOBS
)

DEFAULT_SRC_COMPILE_PARAMS=(
    OPTIONS="${SQLITE_OPTIONS[*]}"
)

DEFAULT_SRC_TEST_PARAMS=(
    OPTIONS="${SQLITE_OPTIONS[*]}"
)

src_prepare() {
    # Do not strip, last checked: 3.49.0
    # only required when building without tests enabled
    ! expecting_tests && edo sed \
        -e 's:$(INSTALL) -s:$(INSTALL):g' \
        -i Makefile.in

    # Fix shell1-5.0 test, last checked: 3.10.0
    # http://mailinglists.sqlite.org/cgi-bin/mailman/private/sqlite-dev/2015-May/002575.html
    sqlite_src_build && edo sed -e "/if {\$i==0x0D /s/\$i==0x0D /&|| (\$i>=0xE0 \&\& \$i<=0xEF) /" \
            -i test/shell1.test
    # Skip tests failing under syd-3.
    sqlite_src_build && expatch "${FILES}"/${PN}-3.45.1-syd3-noroot.patch

    default
}

src_configure() {
    # Required for cross-compilation when building from the full source zip
    export CC_FOR_BUILD=$(exhost --build)-cc
    export BUILD_CFLAGS=$(print-build-flags CFLAGS)

    # The autosetup build doesn't support the OPTIONS variable, but unlike the full source, it
    # passes through CPPFLAGS properly.
    sqlite_src_build || export CPPFLAGS="${CPPFLAGS} ${SQLITE_OPTIONS[*]}"

    local myconf=(
        # uses autosetup instead of autotools
        --hates=datarootdir
        --hates=docdir
        --hates=enable-fast-install
        # set SONAME for the library
        --soname=legacy
        # Support Full-Text Search versions 3,4 and 5.
        # https://sqlite.org/fts3.html
        --enable-fts3 --enable-fts4 --enable-fts5
        # Support the Geopoly interface to R*Tree
        # https://sqlite.org/geopoly.html
        --enable-geopoly
        # Support Built-In Mathematical SQL Functions.
        # https://sqlite.org/compile.html#enable_math_functions
        # https://sqlite.org/lang_mathfunc.html
        --enable-math
        # Support memsys5 memory allocator.
        # https://sqlite.org/malloc.html#memsys5
        --enable-memsys5
        # Support R*Trees
        # https://sqlite.org/rtree.html
        --enable-rtree
        # Support Session extension and pre-update hook functions.
        # https://sqlite.org/compile.html#enable_session
        # https://sqlite.org/sessionintro.html
        # https://sqlite.org/c3ref/preupdate_count.html
        --enable-session
        # Support multithreaded use of sqlite
        --enable-threadsafe
        --disable-editline
        $(option_enable debug)
        $(option_enable readline)
        $(option_enable static)
        $(expecting_tests && echo "--with-tcl=/usr/$(exhost --target)/lib/")
    )

    econf "${myconf[@]}"
}

# NOTE(somasis): tests attempt to link with host tcl when cross-compiling
src_test() {
    if sqlite_src_build ; then
        default
    else
        echo "cross compiling, skipping tests"
    fi
}

src_install() {
    if sqlite_src_build ; then
        emake DESTDIR="${IMAGE}" HAVE_TCL="" install
        doman ${PN}${SLOT}.1
    else
        default
    fi

    if option doc ; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r "${WORKBASE}"/${PN}-doc-${SCHNARF_PV_DOC}/*
    fi
}

