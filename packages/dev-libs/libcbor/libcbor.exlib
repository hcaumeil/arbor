# Copyright 2020 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user="PJK" tag="v${PV}" ]
require cmake
require alternatives

export_exlib_phases src_install

SUMMARY="CBOR protocol implementation for C and others"

LICENCES="MIT"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    test:
        dev-util/cmocka
    run:
        !dev-libs/libcbor:0[<0.9.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS=ON
    -DHUGE_FUZZ:BOOL=FALSE
    -DSANITIZE:BOOL=FALSE
    -DWITH_EXAMPLES:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=( '-DWITH_TESTS:BOOL=TRUE -DWITH_TESTS:BOOL=FALSE' )

libcbor_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/cbor           cbor-${SLOT}
        /usr/${host}/include/cbor.h         cbor-${SLOT}.h
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if ever at_least 0.11.0 ; then
        arch_dependent_alternatives+=(
            /usr/${host}/lib/cmake/${PN} ${PN}-${SLOT}
        )
    fi

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

