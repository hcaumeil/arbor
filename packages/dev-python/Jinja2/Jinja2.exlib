# Copyright 2009 Jan Meier
# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

if ever at_least 3.1.4 ; then
    MY_PN=${PN,}
    MY_PNV=${MY_PN}-${PV}
    require pypi py-pep517 [ backend=flit_core backend_version_spec="[<4]" work=${MY_PNV} test=pytest ]
    MARKUP_SAFE_VER="2.0"
else
    require pypi setup-py [ import=setuptools blacklist=none test=pytest ]
    MARKUP_SAFE_VER="0.23"
fi

SUMMARY="Jinja2 is a template engine written in pure Python"
HOMEPAGE+=" https://palletsprojects.com/p/jinja/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Install HTML documentation ] ]]
"

ever at_least 3.1.4 || MYOPTIONS+=" vim-syntax"

DEPENDENCIES="
    build:
        doc? (
            dev-python/Sphinx[>=2.1.2][python_abis:*(-)?]
            dev-python/sphinx-issues[>=1.2.0][python_abis:*(-)?]
            dev-python/sphinxcontrib-log-cabinet[>=1.0.1][python_abis:*(-)?]
            dev-python/Pallets-Sphinx-Themes[>=2.0.1][python_abis:*(-)?]
        )
    build+run:
        dev-python/MarkupSafe[>=${MARKUP_SAFE_VER}][python_abis:*(-)?]
"

DEFAULT_SRC_INSTALL_EXCLUDE=( PKG-INFO )

prepare_one_multibuild() {
    if ever at_least 3.1.4 ; then
        py-pep517_prepare_one_multibuild
    else
        # upstream doesn't care to fix this, see https://github.com/pallets/jinja/issues/{643,653,655}
        if [[ $(python_get_abi) == 2.7 ]] ; then
            edo rm src/jinja2/async{filters,support}.py
        fi

        setup-py_prepare_one_multibuild
    fi
}

compile_one_multibuild() {
    if ever at_least 3.1.4 ; then
        py-pep517_compile_one_multibuild
    else
        setup-py_compile_one_multibuild
    fi

    option doc && emake -C docs html
}

install_one_multibuild() {
    if ever at_least 3.1.4 ; then
        py-pep517_install_one_multibuild
    else
        setup-py_install_one_multibuild
    fi

    # Install HTML docs excluding intersphinx file
    if option doc; then
        edo pushd docs/_build
        edo rm html/objects.inv
        dodoc -r html
        edo popd
    fi

    if ever at_least 3.1.4 ; then
        :
    else
        if option vim-syntax; then
            insinto /usr/share/vim/vimfiles/syntax
            doins ext/Vim/*
        fi
    fi
}

