# Copyright 2018 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user='universal-ctags' project='ctags' release=v${PV} suffix=tar.gz ]
require alternatives

SUMMARY="Generates an index file of language objects found in source files"
HOMEPAGE="https://ctags.io/ ${HOMEPAGE}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    json [[ description = [ Add support for JSON format as output ] ]]
    pcre [[ description = [ Support for Perl-Compatible Regular Expressions ] ]]
    seccomp [[ description = [ Add the sandbox submode ] ]]
    xml
    yaml [[ description = [ Add support for YAML format as output ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-python/docutils
        virtual/pkg-config
    build+run:
        json? ( dev-libs/jansson )
        pcre? ( dev-libs/pcre2 )
        seccomp? ( sys-libs/libseccomp )
        xml? ( dev-libs/libxml2:2.0[>=2.7.7] )
        yaml? ( dev-libs/libyaml )
"

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-etags
    --enable-tmpdir=/tmp
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'json'
    'pcre pcre2'
    'seccomp'
    'xml'
    'yaml'
)

src_test() {
    emake tmain units tlib check-genfile tutil
}

src_install() {
    default

    # alternatives collision
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{ctags,universal-ctags}
    edo mv "${IMAGE}"/usr/share/man/man1/{ctags,universal-ctags}.1

    alternatives_for ctags universal-ctags 1500 \
        /usr/$(exhost --target)/bin/ctags universal-ctags \
        /usr/share/man/man1/ctags.1 universal-ctags.1
}

