# Copyright 2009-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="A library implementing the SSH2 protocol"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/download/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changes.html [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.html [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: gcrypt libressl mbedtls openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        providers:gcrypt? ( dev-libs/libgcrypt )
        providers:libressl? ( dev-libs/libressl:= )
        providers:mbedtls? ( dev-libs/mbedtls )
        providers:openssl? ( dev-libs/openssl:= )
"

src_configure() {
    local params=(
        -DCMAKE_INSTALL_DOCDIR="/usr/share/doc/${PNVR}"

        -DBUILD_STATIC_LIBS:BOOL=FALSE
        -DBUILD_EXAMPLES:BOOL=FALSE
        -DCLEAR_MEMORY:BOOL=FALSE
        -DENABLE_WERROR:BOOL=FALSE
        -DENABLE_ZLIB_COMPRESSION:BOOL=TRUE
        -DHIDE_SYMBOLS:BOOL=TRUE
        -DLINT:BOOL=FALSE
        -DRUN_DOCKER_TESTS:BOOL=FALSE
        -DRUN_SSHD_TESTS:BOOL=FALSE

        $(expecting_tests '-DBUILD_TESTING:BOOL=TRUE' '-DBUILD_TESTING:BOOL=FALSE')
    )

    if option providers:gcrypt; then
        params+=( -DCRYPTO_BACKEND="Libgcrypt" )
    elif option providers:mbedtls; then
        params+=( -DCRYPTO_BACKEND="mbedTLS" )
    else
        params+=( -DCRYPTO_BACKEND="OpenSSL" )
    fi

    ecmake "${params[@]}"
}

