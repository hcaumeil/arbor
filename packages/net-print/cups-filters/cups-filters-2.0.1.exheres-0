# Copyright 2012-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=OpenPrinting release=${PV} suffix=tar.xz ]
require flag-o-matic

SUMMARY="OpenPrinting CUPS filters"
DESCRIPTION="
This distribution contains backends, filters, and other software that was
once part of the core CUPS distribution but is no longer maintained by
Apple Inc. In addition it contains additional filters developed
independently of Apple, especially filters for the PDF-centric printing
workflow introduced by OpenPrinting.
"
HOMEPAGE+=" https://wiki.linuxfoundation.org/openprinting/${PN}"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config
    build+run:
        dev-libs/libcupsfilters[>=2.0.0]
        dev-libs/libppd[>=2.0.0]
        net-print/cups[>=2.2.2]
    suggestion:
        net-print/foomatic-db [[ description = [ for non-PostScript printers ] ]]
"

src_configure() {
    # TODO: gcc 15 defaults to C23, which disallows unprotoyped functions
    # https://github.com/OpenPrinting/cups-filters/issues/605
    append-cflags -std=gnu17

    local myconf=(
        --enable-driverless
        --enable-foomatic
        --enable-ghostscript
        --enable-imagefilters
        --enable-nls
        --enable-poppler
        --enable-universal-cups-filter
        --disable-individual-cups-filters
        --disable-mutool
        --disable-pstops
        --disable-rastertopwg
        --disable-static
        --disable-werror
        --with-cups-config=/usr/$(exhost --target)/bin/cups-config
        --with-fontdir=fonts/conf.avail
        --with-gs-path=system
        --with-ippfind-path=system
        --with-shell=/usr/$(exhost --target)/bin/bash
    )

    econf "${myconf[@]}"
}

