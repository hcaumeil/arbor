# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.gz ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Groff produces formatted output from plain text"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    X
    parts: binaries configuration data development documentation libraries
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.6.1]
    build+run:
        X? (
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libX11
            x11-libs/libXaw
            x11-libs/libXmu
            x11-libs/libXt
        )
    suggestion:
        (
            dev-perl/File-HomeDir
            media-sound/lilypond
        ) [[ *description = [ Integrate lilypond parts into groff ] *group-name = [ lilypond ] ]]
"

AT_M4DIR=( m4 gnulib_m4 )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.20.1-man-unicode-dashes.patch
    "${FILES}"/${PN}-1.23.0-load-site-font-and-site-tmac-from-etc-groff.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-appdefdir=/usr/share/X11/app-defaults
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'X x'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( BUG-REPORT FDL 'INSTALL*' MORE.STUFF PROJECTS REVISION VERSION )

src_compile() {
    if exhost --is-native -q; then
        emake
    else
        emake \
            GROFFBIN=/usr/$(exhost --build)/bin/groff \
            TROFFBIN=/usr/$(exhost --build)/bin/troff \
            GROFF_BIN_PATH=""
    fi
}

src_install() {
    default

    edo rmdir "${IMAGE}"/etc/groff/site-font

    # TODO(compnerd) determine why these symlinks are required
    dosym tbl /usr/$(exhost --target)/bin/gtbl
    dosym eqn /usr/$(exhost --target)/bin/geqn

    expart binaries /usr/$(exhost --target)/bin
    expart configuration /etc
    expart data /usr/share/{groff,X11}
    expart documentation /usr/share/{doc,info,man}
    expart libraries /usr/$(exhost --target)/lib
}

