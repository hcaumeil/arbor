# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Collection of Linux man pages"
HOMEPAGE="https://www.kernel.org/doc/${PN}"
DOWNLOADS="mirror://kernel/linux/docs/${PN}/${PNV}.tar.xz"

LICENCES="${PN}"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run:
        virtual/man
    suggestion:
        sys-apps/man-pages-posix
"

DEFAULT_SRC_INSTALL_PARAMS=(
    bindir=/usr/$(exhost --target)/bin
    datarootdir=/usr/share
)

src_compile() {
    # has a Makefile with various targets, but none that we want
    :
}

src_test() {
    # There are currently some warnings about man pages being wider than
    # columns, so only enable tests that "work reliably"
    # https://lore.kernel.org/linux-man/0dfd5319-2d22-a8ad-f085-d635eb6d0678@gmail.com/T/#ma7cc17653b6918102ed4d78621f046cc97331b0b
    emake lint-man-tbl
}

src_install() {
    default

    # avoid collision with timezone-data
    edo rm "${IMAGE}"/usr/share/man/man5/tzfile.5
    for i in tzselect zdump zic; do
        edo rm "${IMAGE}"/usr/share/man/man8/${i}.8
    done

    # avoid collision with dev-libs/libxcrypt
    edo rm "${IMAGE}"/usr/share/man/man3/crypt{,_r}.3
}

