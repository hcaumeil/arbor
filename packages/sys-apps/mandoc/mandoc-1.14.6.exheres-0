# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="A suite of tools compiling mdoc, the roff macro language for BSD manual pages, and man, the historical language for UNIX manuals"
HOMEPAGE="https://mdocml.bsd.lv"
DOWNLOADS="${HOMEPAGE}/snapshots/${PNV}.tar.gz"

LICENCES="BSD-3 ISC"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES=""

src_configure() {
	edo sed 's/\tar /\t$(AR) /' -i Makefile
    cat > configure.local <<EOF
PREFIX="/usr"
BINDIR="/usr/$(exhost --target)/libexec/${PN}"
SBINDIR="/usr/$(exhost --target)/libexec/${PN}"
BIN_FROM_SBIN="${IMAGE}/usr/$(exhost --target)/libexec/${PN}"
INCLUDEDIR="/usr/$(exhost --target)/include"
LIBDIR="/usr/$(exhost --target)/lib"
MANDIR="/usr/share/man"

MANPATH_DEFAULT="/usr/share/man:/usr/local/share/man"
OSNAME="Exherbo Linux"

# We want to allow other man implementations to coexist
MANM_MANCONF="mandoc.conf"

CFLAGS+=" ${CFLAGS}"
STATIC=

AR=$(exhost --tool-prefix)ar
CC=$(exhost --tool-prefix)cc
EOF
    edo ./configure
}

src_test() {
    emake regress
}

src_install() {
    default

    # conflicts with sys-apps/groff
    edo mv "${IMAGE}"/usr/share/man/man7/{,mandoc.}roff.7
    edo mv "${IMAGE}"/usr/share/man/man1/{,mandoc.}soelim.1

    # conflicts with sys-apps/man-pages
    edo mv "${IMAGE}"/usr/share/man/man7/{,mandoc.}man.7
    edo mv "${IMAGE}"/usr/share/man/man7/{,mandoc.}mdoc.7

    insinto /etc
    doins "${FILES}"/mandoc.conf

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/bin
    edo pushd "${IMAGE}"/usr/$(exhost --target)/bin
    edo ln -s /usr/$(exhost --target)/libexec/mandoc/mandoc mandoc
    for bin in apropos makewhatis man whatis;do
        herebin ${PN}.${bin} <<EOF
#!/bin/sh
exec /usr/$(exhost --target)/libexec/${PN}/${bin} "\$@"
EOF
    done
    edo popd
    alternatives_for man ${PN} 500  \
        /usr/$(exhost --target)/bin/apropos     ${PN}.apropos       \
        /usr/$(exhost --target)/bin/makewhatis  ${PN}.makewhatis    \
        /usr/$(exhost --target)/bin/man         ${PN}.man           \
        /usr/$(exhost --target)/bin/whatis      ${PN}.whatis        \
        /usr/share/man/man1/apropos.1           ${PN}.apropos.1     \
        /usr/share/man/man1/man.1               ${PN}.man.1         \
        /usr/share/man/man1/whatis.1            ${PN}.whatis.1      \
        /usr/share/man/man8/makewhatis.8        ${PN}.makewhatis.8
}

pkg_postinst() {
    if exhost --is-native -q ;then
        edo /usr/$(exhost --target)/libexec/mandoc/makewhatis -a -T utf8 /usr/share/man
    fi
}

