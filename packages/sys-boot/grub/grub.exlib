# Copyright 2007 Bryan Østergaard <bryan.ostergaard@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] \
    flag-o-matic \
    bash-completion

export_exlib_phases pkg_setup src_prepare src_configure src_install pkg_postinst

SUMMARY="GRUB (Grand Unified Boot) bootloader"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    device-mapper [[ description = [ Ability to detect and use device-mapper devices ] ]]
    efi           [[ description = [ Build for EFI platform ] ]]
    grub-mount    [[ description = [ FUSE driver for filesystems that GRUB understands ] ]]
    mkfont        [[ description = [ Build grub-mkfont which can create GRUB font files ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( linguas: ast ca da de de_CH eo es fi fr gl he hr hu id it ja ka ko lg lt nb nl pa pl pt pt_BR
               ro ru sl sr sv tr uk vi zh_CN zh_TW )
"

# TODO: needs qemu-system-i386
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*
        sys-apps/help2man [[ note = [ man pages won't get created otherwise resulting in empty man dirs ] ]]
        sys-devel/bison
        sys-devel/flex[>=2.5.35]
        sys-devel/make
        virtual/pkg-config
        device-mapper? ( sys-fs/lvm2 )
        grub-mount? ( sys-fs/fuse:0 )
        mkfont? (
            fonts/unifont
            media-libs/freetype:2
        )
    build+run:
        app-arch/xz
        sys-devel/gettext[>=0.18.3]
        !sys-boot/grub-static [[ description = [ /sbin/grub-install collides ] resolution = manual ]]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        efi? ( sys-boot/efibootmgr )
    suggestion:
        sys-boot/os-prober [[
            description = [ Ability to add other OSs automatically to grub.cfg via grub-mkconf ]
        ]]
"

grub_pkg_setup() {
    # grub2 should be compiled with -Os, other optimizations may result in oversized core images
    replace-flags -O* -Os
    # retpoline flag is incompatible with the -mcmodel=large flag used by grub
    filter-flags -mindirect-branch=thunk
    # upstream doesn't support -march other than generic, fails to compile with e.g. -march=znver2
    # https://savannah.gnu.org/bugs/?56978
    if [[ $(exhost --target) == x86_64-pc-linux-gnu ]] ; then
        replace-flags -march* -march=x86-64
    elif [[ $(exhost --target) == i686-pc-linux-gnu ]] ; then
        replace-flags -march* -march=i386
    fi
}

grub_src_prepare() {
    # fix build, the empty file is missing from the release tarball
    # https://git.savannah.gnu.org/cgit/grub.git/commit/?id=154dcb1aea9f8fc42b2bce98bebed004d7783a7d
    edo touch grub-core/extra_deps.lst

    default
}

grub_src_configure() {
    filter-flags -mfloat-abi=hard

    myconf=(
        "CPP=${CC} -E"
        BUILD_CC=$(exhost --build)-cc
        BUILD_CFLAGS="$(print-build-flags CFLAGS)"
        BUILD_CPPFLAGS="$(print-build-flags CPPFLAGS)"
        BUILD_LDFLAGS="$(print-build-flags LDFLAGS)"
        --enable-cache-stats
        --enable-nls
        --enable-year2038
        --disable-libzfs
        --disable-stack-protector
        --disable-werror
        --with-bootdir=/boot
        --with-grubdir=grub
        # TODO: for now disable emulation utils
        --disable-efiemu
        --disable-grub-emu-sdl
        --disable-grub-emu-sdl2
        --disable-grub-emu-pci
        --disable-mm-debug
        gt_cv_func_gnugettext{1,2}_libc=yes
    )

    option efi && myconf+=( '--with-platform=efi' )

    econf "${myconf[@]}" \
        $(option_enable device-mapper) \
        $(option_enable grub-mount) \
        $(option_enable mkfont grub-mkfont)
}

grub_src_install() {
    default

    insinto /etc/default
    newins "${FILES}"/grub.default grub

    dodoc "${WORK}"/docs/grub.cfg

    edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/charset.alias

    edo rm "${IMAGE%/}"/etc/bash_completion.d/${PN}
    edo rmdir "${IMAGE%/}"/etc/bash_completion.d/
    dobashcompletion "${WORK%/}"/util/bash-completion.d/${PN}
}

grub_pkg_postinst() {
    elog "Use grub-install /dev/your-device e.g. sda to install grub onto the"
    elog "device you wish to boot from."
    elog "A sample grub.cfg has been installed into /usr/share/doc/${PNVR}."
    elog "Copy this to /boot/grub and edit it to fit your configuration."
    elog "Alternatively, you can use the provided grub-mkconfig utility."
    elog "Due to security concerns, os-prober support is disabled by default."
    elog "Set GRUB_DISABLE_OS_PROBER=false in /etc/default/grub to enable it."
}

