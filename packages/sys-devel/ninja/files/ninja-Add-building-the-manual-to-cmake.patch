Upstream: under review, https://github.com/ninja-build/ninja/pull/2423

From 127d867b968eeaa1a98aca055dc6c849bbc9ab33 Mon Sep 17 00:00:00 2001
From: Heiko Becker <mail@heiko-becker.de>
Date: Tue, 16 Apr 2024 23:44:18 +0200
Subject: [PATCH] Add building the manual to cmake

Also raise cmake_minimum_required to 3.18 for the LibXslt::xsltproc
target.
---
 CMakeLists.txt     | 15 ++++++++++++++-
 doc/CMakeLists.txt | 17 +++++++++++++++++
 2 files changed, 31 insertions(+), 1 deletion(-)
 create mode 100644 doc/CMakeLists.txt

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 337d4f1..1ab096b 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -1,9 +1,10 @@
-cmake_minimum_required(VERSION 3.15)
+cmake_minimum_required(VERSION 3.18)
 
 include(CheckSymbolExists)
 include(CheckIPOSupported)
 
 option(NINJA_BUILD_BINARY "Build ninja binary" ON)
+option(NINJA_BUILD_DOC "Build ninja's manual" ON)
 option(NINJA_FORCE_PSELECT "Use pselect() even on platforms that provide ppoll()" OFF)
 
 project(ninja CXX)
@@ -122,6 +123,18 @@ set(NINJA_PYTHON "python" CACHE STRING "Python interpreter to use for the browse
 
 check_platform_supports_browse_mode(platform_supports_ninja_browse)
 
+if(NINJA_BUILD_DOC)
+	find_program(Asciidoc_Executable asciidoc)
+
+	find_package(LibXslt REQUIRED)
+
+	if(NOT Asciidoc_Executable OR NOT TARGET LibXslt::xsltproc)
+		message(SEND_ERROR "asciidoc and xsltproc are required to build the documentation")
+	else()
+		add_subdirectory(doc)
+	endif()
+endif()
+
 # Core source files all build into ninja library.
 add_library(libninja OBJECT
 	src/build_log.cc
diff --git a/doc/CMakeLists.txt b/doc/CMakeLists.txt
new file mode 100644
index 0000000..dd880de
--- /dev/null
+++ b/doc/CMakeLists.txt
@@ -0,0 +1,17 @@
+add_custom_command(OUTPUT
+		"${CMAKE_CURRENT_BINARY_DIR}/manual.xml"
+	COMMAND
+		asciidoc --backend=docbook --doctype=book --out-file "${CMAKE_CURRENT_BINARY_DIR}/manual.xml" "${CMAKE_CURRENT_SOURCE_DIR}/manual.asciidoc"
+)
+
+add_custom_command(DEPENDS
+		"${CMAKE_CURRENT_BINARY_DIR}/manual.xml"
+	OUTPUT
+		"${CMAKE_CURRENT_BINARY_DIR}/manual.html"
+	COMMAND
+		LibXslt::xsltproc --nonet "${CMAKE_CURRENT_SOURCE_DIR}/docbook.xsl" manual.xml > "${CMAKE_CURRENT_BINARY_DIR}/manual.html"
+)
+
+add_custom_target(doc ALL DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/manual.html")
+
+install(FILES "${CMAKE_CURRENT_BINARY_DIR}/manual.html" TYPE DOC)
-- 
2.44.0

