From 702659e57b2839153eb02dbd6aa51ea89c19c817 Mon Sep 17 00:00:00 2001
From: Andreas Sturmlechner <asturm@gentoo.org>
Date: Sun, 16 Jun 2019 10:59:46 +0200
Subject: [PATCH 1/2] Add USE_SYSTEM_LIBS option to build without bundled libs

headers: s/vendor_cryptopp/cryptopp/

Only gtest and crypto++ are being unbundled. In release/0.10 branch,
bundled spdlog version is too old for Gentoo to satisfy with system-lib.
---
 CMakeLists.txt                                    | 15 ++++++++++++++-
 .../compressing/compressors/Gzip.cpp              |  2 +-
 src/cpp-utils/CMakeLists.txt                      |  6 +++++-
 src/cpp-utils/crypto/hash/Hash.cpp                |  2 +-
 src/cpp-utils/crypto/kdf/Scrypt.cpp               |  2 +-
 src/cpp-utils/crypto/symmetric/CFB_Cipher.h       |  2 +-
 src/cpp-utils/crypto/symmetric/GCM_Cipher.h       |  2 +-
 src/cpp-utils/crypto/symmetric/ciphers.h          | 12 ++++++------
 src/cpp-utils/data/Data.cpp                       |  2 +-
 src/cpp-utils/data/FixedSizeData.h                |  2 +-
 src/cpp-utils/random/OSRandomGenerator.h          |  2 +-
 src/cpp-utils/random/RandomGeneratorThread.h      |  2 +-
 src/cryfs/impl/localstate/BasedirMetadata.cpp     |  2 +-
 test/blobstore/CMakeLists.txt                     |  2 +-
 test/blockstore/CMakeLists.txt                    |  2 +-
 test/cpp-utils/CMakeLists.txt                     |  2 +-
 test/cryfs-cli/CMakeLists.txt                     |  2 +-
 test/cryfs/CMakeLists.txt                         |  2 +-
 test/cryfs/impl/config/CompatibilityTest.cpp      |  2 +-
 test/fspp/CMakeLists.txt                          |  2 +-
 test/gitversion/CMakeLists.txt                    |  2 +-
 test/my-gtest-main/CMakeLists.txt                 |  2 +-
 test/parallelaccessstore/CMakeLists.txt           |  2 +-
 23 files changed, 45 insertions(+), 28 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 9a0e9db5..51f7f641 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -19,6 +19,7 @@ require_clang_version(7.0)
 option(BUILD_TESTING "build test cases" OFF)
 option(CRYFS_UPDATE_CHECKS "let cryfs check for updates and security vulnerabilities" ON)
 option(DISABLE_OPENMP "allow building without OpenMP libraries. This will cause performance degradations." OFF)
+option(USE_SYSTEM_LIBS "build with system libs instead of bundled libs" OFF)
 
 # The following options are helpful for development and/or CI
 option(USE_WERROR "build with -Werror flag")
@@ -46,7 +47,19 @@ endif()
 
 include(${DEPENDENCY_CONFIG})
 
-add_subdirectory(vendor EXCLUDE_FROM_ALL)
+if(USE_SYSTEM_LIBS)
+    include(FindPkgConfig)
+    pkg_check_modules(CRYPTOPP REQUIRED libcryptopp>=8.2)
+    add_subdirectory(vendor/spdlog EXCLUDE_FROM_ALL)
+    if(BUILD_TESTING)
+        find_package(GTest CONFIG REQUIRED)
+        set(GOOGLETEST_LIBS GTest::gtest GTest::gmock)
+    endif()
+else()
+    add_subdirectory(vendor EXCLUDE_FROM_ALL)
+    set(GOOGLETEST_LIBS googletest)
+endif()
+
 add_subdirectory(src)
 add_subdirectory(doc)
 add_subdirectory(test)
diff --git a/src/blockstore/implementations/compressing/compressors/Gzip.cpp b/src/blockstore/implementations/compressing/compressors/Gzip.cpp
index 5420ebf5..5f169cf6 100644
--- a/src/blockstore/implementations/compressing/compressors/Gzip.cpp
+++ b/src/blockstore/implementations/compressing/compressors/Gzip.cpp
@@ -1,5 +1,5 @@
 #include "Gzip.h"
-#include <vendor_cryptopp/gzip.h>
+#include <cryptopp/gzip.h>
 
 using cpputils::Data;
 
diff --git a/src/cpp-utils/CMakeLists.txt b/src/cpp-utils/CMakeLists.txt
index dbe2519e..9c488370 100644
--- a/src/cpp-utils/CMakeLists.txt
+++ b/src/cpp-utils/CMakeLists.txt
@@ -89,7 +89,11 @@ target_link_libraries(${PROJECT_NAME} PUBLIC ${CMAKE_THREAD_LIBS_INIT})
 
 target_link_libraries(${PROJECT_NAME} PUBLIC ${CMAKE_DL_LIBS})
 
-target_link_libraries(${PROJECT_NAME} PUBLIC CryfsDependencies_spdlog cryptopp CryfsDependencies_range-v3)
+if(USE_SYSTEM_LIBS)
+    target_link_libraries(${PROJECT_NAME} PUBLIC CryfsDependencies_spdlog ${CRYPTOPP_LIBRARIES} CryfsDependencies_range-v3)
+else()
+    target_link_libraries(${PROJECT_NAME} PUBLIC CryfsDependencies_spdlog cryptopp CryfsDependencies_range-v3)
+endif()
 
 target_add_boost(${PROJECT_NAME})
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/src/cpp-utils/crypto/hash/Hash.cpp b/src/cpp-utils/crypto/hash/Hash.cpp
index 696cdeaf..e07d28da 100644
--- a/src/cpp-utils/crypto/hash/Hash.cpp
+++ b/src/cpp-utils/crypto/hash/Hash.cpp
@@ -1,6 +1,6 @@
 #include "Hash.h"
 #include <cpp-utils/random/Random.h>
-#include <vendor_cryptopp/sha.h>
+#include <cryptopp/sha.h>
 
 using cpputils::Random;
 using CryptoPP::SHA512;
diff --git a/src/cpp-utils/crypto/kdf/Scrypt.cpp b/src/cpp-utils/crypto/kdf/Scrypt.cpp
index 1fec96ce..a12b6369 100644
--- a/src/cpp-utils/crypto/kdf/Scrypt.cpp
+++ b/src/cpp-utils/crypto/kdf/Scrypt.cpp
@@ -1,5 +1,5 @@
 #include "Scrypt.h"
-#include <vendor_cryptopp/scrypt.h>
+#include <cryptopp/scrypt.h>
 
 using std::string;
 
diff --git a/src/cpp-utils/crypto/symmetric/CFB_Cipher.h b/src/cpp-utils/crypto/symmetric/CFB_Cipher.h
index c1a8aa1c..c0e3d8c5 100644
--- a/src/cpp-utils/crypto/symmetric/CFB_Cipher.h
+++ b/src/cpp-utils/crypto/symmetric/CFB_Cipher.h
@@ -6,7 +6,7 @@
 #include "../../data/Data.h"
 #include "../../random/Random.h"
 #include <boost/optional.hpp>
-#include <vendor_cryptopp/modes.h>
+#include <cryptopp/modes.h>
 #include "Cipher.h"
 #include "EncryptionKey.h"
 
diff --git a/src/cpp-utils/crypto/symmetric/GCM_Cipher.h b/src/cpp-utils/crypto/symmetric/GCM_Cipher.h
index 9cf6d53f..86b2b8e3 100644
--- a/src/cpp-utils/crypto/symmetric/GCM_Cipher.h
+++ b/src/cpp-utils/crypto/symmetric/GCM_Cipher.h
@@ -3,7 +3,7 @@
 #define MESSMER_CPPUTILS_CRYPTO_SYMMETRIC_GCMCIPHER_H_
 
 #include "AEAD_Cipher.h"
-#include <vendor_cryptopp/gcm.h>
+#include <cryptopp/gcm.h>
 
 namespace cpputils {
 
diff --git a/src/cpp-utils/crypto/symmetric/ciphers.h b/src/cpp-utils/crypto/symmetric/ciphers.h
index 0bae6866..eee3111e 100644
--- a/src/cpp-utils/crypto/symmetric/ciphers.h
+++ b/src/cpp-utils/crypto/symmetric/ciphers.h
@@ -2,12 +2,12 @@
 #ifndef MESSMER_CPPUTILS_CRYPTO_SYMMETRIC_CIPHERS_H_
 #define MESSMER_CPPUTILS_CRYPTO_SYMMETRIC_CIPHERS_H_
 
-#include <vendor_cryptopp/aes.h>
-#include <vendor_cryptopp/twofish.h>
-#include <vendor_cryptopp/serpent.h>
-#include <vendor_cryptopp/cast.h>
-#include <vendor_cryptopp/mars.h>
-#include <vendor_cryptopp/chachapoly.h>
+#include <cryptopp/aes.h>
+#include <cryptopp/twofish.h>
+#include <cryptopp/serpent.h>
+#include <cryptopp/cast.h>
+#include <cryptopp/mars.h>
+#include <cryptopp/chachapoly.h>
 #include "GCM_Cipher.h"
 #include "CFB_Cipher.h"
 
diff --git a/src/cpp-utils/data/Data.cpp b/src/cpp-utils/data/Data.cpp
index be94cdbe..e283f0eb 100644
--- a/src/cpp-utils/data/Data.cpp
+++ b/src/cpp-utils/data/Data.cpp
@@ -1,6 +1,6 @@
 #include "Data.h"
 #include <stdexcept>
-#include <vendor_cryptopp/hex.h>
+#include <cryptopp/hex.h>
 
 using std::istream;
 using std::ofstream;
diff --git a/src/cpp-utils/data/FixedSizeData.h b/src/cpp-utils/data/FixedSizeData.h
index 58833996..17891c95 100644
--- a/src/cpp-utils/data/FixedSizeData.h
+++ b/src/cpp-utils/data/FixedSizeData.h
@@ -2,7 +2,7 @@
 #ifndef MESSMER_CPPUTILS_DATA_FIXEDSIZEDATA_H_
 #define MESSMER_CPPUTILS_DATA_FIXEDSIZEDATA_H_
 
-#include <vendor_cryptopp/hex.h>
+#include <cryptopp/hex.h>
 #include <string>
 #include <array>
 #include <cstring>
diff --git a/src/cpp-utils/random/OSRandomGenerator.h b/src/cpp-utils/random/OSRandomGenerator.h
index f522c617..d99f977d 100644
--- a/src/cpp-utils/random/OSRandomGenerator.h
+++ b/src/cpp-utils/random/OSRandomGenerator.h
@@ -3,7 +3,7 @@
 #define MESSMER_CPPUTILS_RANDOM_OSRANDOMGENERATOR_H
 
 #include "RandomGenerator.h"
-#include <vendor_cryptopp/osrng.h>
+#include <cryptopp/osrng.h>
 
 namespace cpputils {
     class OSRandomGenerator final : public RandomGenerator {
diff --git a/src/cpp-utils/random/RandomGeneratorThread.h b/src/cpp-utils/random/RandomGeneratorThread.h
index 593750ed..103c00d7 100644
--- a/src/cpp-utils/random/RandomGeneratorThread.h
+++ b/src/cpp-utils/random/RandomGeneratorThread.h
@@ -4,7 +4,7 @@
 
 #include "../thread/LoopThread.h"
 #include "ThreadsafeRandomDataBuffer.h"
-#include <vendor_cryptopp/osrng.h>
+#include <cryptopp/osrng.h>
 
 namespace cpputils {
     //TODO Test
diff --git a/src/cryfs/impl/localstate/BasedirMetadata.cpp b/src/cryfs/impl/localstate/BasedirMetadata.cpp
index d32ced93..3de2d3ad 100644
--- a/src/cryfs/impl/localstate/BasedirMetadata.cpp
+++ b/src/cryfs/impl/localstate/BasedirMetadata.cpp
@@ -1,7 +1,7 @@
 #include "BasedirMetadata.h"
 #include <boost/property_tree/ptree.hpp>
 #include <boost/property_tree/json_parser.hpp>
-#include <vendor_cryptopp/sha.h>
+#include <cryptopp/sha.h>
 #include <boost/filesystem/operations.hpp>
 #include "LocalStateDir.h"
 #include <cpp-utils/logging/logging.h>
diff --git a/test/blobstore/CMakeLists.txt b/test/blobstore/CMakeLists.txt
index 05e98b8d..342d5626 100644
--- a/test/blobstore/CMakeLists.txt
+++ b/test/blobstore/CMakeLists.txt
@@ -27,7 +27,7 @@ set(SOURCES
 )
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest blobstore)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} blobstore)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/test/blockstore/CMakeLists.txt b/test/blockstore/CMakeLists.txt
index ca63acce..6dc5f505 100644
--- a/test/blockstore/CMakeLists.txt
+++ b/test/blockstore/CMakeLists.txt
@@ -42,7 +42,7 @@ set(SOURCES
 )
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest blockstore)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} blockstore)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/test/cpp-utils/CMakeLists.txt b/test/cpp-utils/CMakeLists.txt
index a13ad986..6f4fb93b 100644
--- a/test/cpp-utils/CMakeLists.txt
+++ b/test/cpp-utils/CMakeLists.txt
@@ -71,7 +71,7 @@ target_activate_cpp14(${PROJECT_NAME}_exit_signal)
 target_link_libraries(${PROJECT_NAME}_exit_signal cpp-utils)
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest cpp-utils)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} cpp-utils)
 add_dependencies(${PROJECT_NAME} ${PROJECT_NAME}_exit_status ${PROJECT_NAME}_exit_signal)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
diff --git a/test/cryfs-cli/CMakeLists.txt b/test/cryfs-cli/CMakeLists.txt
index 2d0b38c5..76fd75bc 100644
--- a/test/cryfs-cli/CMakeLists.txt
+++ b/test/cryfs-cli/CMakeLists.txt
@@ -16,7 +16,7 @@ set(SOURCES
 )
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest cryfs-cli cryfs-unmount fspp-fuse)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} cryfs-cli cryfs-unmount fspp-fuse)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/test/cryfs/CMakeLists.txt b/test/cryfs/CMakeLists.txt
index eb13b273..0086e6be 100644
--- a/test/cryfs/CMakeLists.txt
+++ b/test/cryfs/CMakeLists.txt
@@ -24,7 +24,7 @@ set(SOURCES
 )
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest cryfs)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} cryfs)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/test/cryfs/impl/config/CompatibilityTest.cpp b/test/cryfs/impl/config/CompatibilityTest.cpp
index 07b931b3..a045900f 100644
--- a/test/cryfs/impl/config/CompatibilityTest.cpp
+++ b/test/cryfs/impl/config/CompatibilityTest.cpp
@@ -2,7 +2,7 @@
 #include <vector>
 #include <boost/filesystem.hpp>
 #include <cpp-utils/data/Data.h>
-#include <vendor_cryptopp/hex.h>
+#include <cryptopp/hex.h>
 #include <cpp-utils/crypto/symmetric/ciphers.h>
 #include <cpp-utils/tempfile/TempFile.h>
 #include <cryfs/impl/config/CryConfigFile.h>
diff --git a/test/fspp/CMakeLists.txt b/test/fspp/CMakeLists.txt
index c6f2e32c..295fe58e 100644
--- a/test/fspp/CMakeLists.txt
+++ b/test/fspp/CMakeLists.txt
@@ -103,7 +103,7 @@ set(SOURCES
         testutils/OpenFileHandle.cpp testutils/OpenFileHandle.h)
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest fspp-interface fspp-fuse)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} fspp-interface fspp-fuse)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/test/gitversion/CMakeLists.txt b/test/gitversion/CMakeLists.txt
index 51a5ccc1..396289fa 100644
--- a/test/gitversion/CMakeLists.txt
+++ b/test/gitversion/CMakeLists.txt
@@ -6,7 +6,7 @@ set(SOURCES
 )
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest gitversion)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} gitversion)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
diff --git a/test/my-gtest-main/CMakeLists.txt b/test/my-gtest-main/CMakeLists.txt
index 1d1e7e08..de4fd107 100644
--- a/test/my-gtest-main/CMakeLists.txt
+++ b/test/my-gtest-main/CMakeLists.txt
@@ -5,7 +5,7 @@ set(SOURCES
 )
 
 add_library(${PROJECT_NAME} STATIC ${SOURCES})
-target_link_libraries(${PROJECT_NAME} PUBLIC googletest cpp-utils)
+target_link_libraries(${PROJECT_NAME} PUBLIC ${GOOGLETEST_LIBS} cpp-utils)
 target_add_boost(${PROJECT_NAME} filesystem system)
 target_include_directories(${PROJECT_NAME} PUBLIC .)
 
diff --git a/test/parallelaccessstore/CMakeLists.txt b/test/parallelaccessstore/CMakeLists.txt
index 16170d17..97379304 100644
--- a/test/parallelaccessstore/CMakeLists.txt
+++ b/test/parallelaccessstore/CMakeLists.txt
@@ -6,7 +6,7 @@ set(SOURCES
 )
 
 add_executable(${PROJECT_NAME} ${SOURCES})
-target_link_libraries(${PROJECT_NAME} my-gtest-main googletest parallelaccessstore)
+target_link_libraries(${PROJECT_NAME} my-gtest-main ${GOOGLETEST_LIBS} parallelaccessstore)
 add_test(${PROJECT_NAME} ${PROJECT_NAME})
 
 target_enable_style_warnings(${PROJECT_NAME})
-- 
2.41.0


From 02c44dd132b8d056719cc314988b49908cb95621 Mon Sep 17 00:00:00 2001
From: Andreas Sturmlechner <asturm@gentoo.org>
Date: Sun, 16 Jun 2019 15:17:16 +0200
Subject: [PATCH 2/2] Use FeatureSummary

---
 CMakeLists.txt | 6 ++++--
 1 file changed, 4 insertions(+), 2 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 51f7f641..b7106782 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -11,6 +11,7 @@ project(cryfs)
 
 list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake-utils)
 include(utils)
+include(FeatureSummary)
 
 require_gcc_version(7.0)
 require_clang_version(7.0)
@@ -49,8 +50,7 @@ include(${DEPENDENCY_CONFIG})
 
 if(USE_SYSTEM_LIBS)
     include(FindPkgConfig)
-    pkg_check_modules(CRYPTOPP REQUIRED libcryptopp>=8.2)
-    add_subdirectory(vendor/spdlog EXCLUDE_FROM_ALL)
+    pkg_check_modules(CRYPTOPP REQUIRED libcryptopp>=8.6)
     if(BUILD_TESTING)
         find_package(GTest CONFIG REQUIRED)
         set(GOOGLETEST_LIBS GTest::gtest GTest::gmock)
@@ -64,3 +64,5 @@ add_subdirectory(src)
 add_subdirectory(doc)
 add_subdirectory(test)
 add_subdirectory(cpack)
+
+feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
-- 
2.41.0

