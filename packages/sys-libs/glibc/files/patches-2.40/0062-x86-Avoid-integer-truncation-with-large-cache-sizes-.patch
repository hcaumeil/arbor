Upstream: yes, taken from release/2.40/master

From 94e4a8c7d68129075f1b494a0b26151a4f989b36 Mon Sep 17 00:00:00 2001
From: Florian Weimer <fweimer@redhat.com>
Date: Tue, 17 Dec 2024 18:12:03 +0100
Subject: [PATCH 62/62] x86: Avoid integer truncation with large cache sizes
 (bug 32470)

Some hypervisors report 1 TiB L3 cache size.  This results
in some variables incorrectly getting zeroed, causing crashes
in memcpy/memmove because invariants are violated.

(cherry picked from commit 61c3450db96dce96ad2b24b4f0b548e6a46d68e5)
---
 NEWS                       | 1 +
 sysdeps/x86/dl-cacheinfo.h | 4 ++--
 2 files changed, 3 insertions(+), 2 deletions(-)

diff --git a/NEWS b/NEWS
index bd0b3bd66a..97a1e1f5d4 100644
--- a/NEWS
+++ b/NEWS
@@ -20,6 +20,7 @@ The following bugs are resolved with this release:
   [32137] libio: Attempt wide backup free only for non-legacy code
   [32231] elf: Change ldconfig auxcache magic number
   [32245] glibc -Wstringop-overflow= build failure on hppa
+  [32470] x86: Avoid integer truncation with large cache sizes
 
 Version 2.40
 
diff --git a/sysdeps/x86/dl-cacheinfo.h b/sysdeps/x86/dl-cacheinfo.h
index a1c03b8903..ac97414b5b 100644
--- a/sysdeps/x86/dl-cacheinfo.h
+++ b/sysdeps/x86/dl-cacheinfo.h
@@ -961,11 +961,11 @@ dl_init_cacheinfo (struct cpu_features *cpu_features)
     non_temporal_threshold = maximum_non_temporal_threshold;
 
   /* NB: The REP MOVSB threshold must be greater than VEC_SIZE * 8.  */
-  unsigned int minimum_rep_movsb_threshold;
+  unsigned long int minimum_rep_movsb_threshold;
   /* NB: The default REP MOVSB threshold is 4096 * (VEC_SIZE / 16) for
      VEC_SIZE == 64 or 32.  For VEC_SIZE == 16, the default REP MOVSB
      threshold is 2048 * (VEC_SIZE / 16).  */
-  unsigned int rep_movsb_threshold;
+  unsigned long int rep_movsb_threshold;
   if (CPU_FEATURE_USABLE_P (cpu_features, AVX512F)
       && !CPU_FEATURE_PREFERRED_P (cpu_features, Prefer_No_AVX512))
     {
-- 
2.47.1

