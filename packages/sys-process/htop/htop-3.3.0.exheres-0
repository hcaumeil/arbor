# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=htop-dev ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="An interactive process viewer for Linux"
HOMEPAGE="https://htop.dev/ ${HOMEPAGE}"

REMOTE_IDS="freshcode:${PN}"

UPSTREAM_CHANGELOG="https://github.com/htop-dev/${PN}/blob/master/ChangeLog"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    backtrace [[ description = [ Enable support for printing backtraces ] ]]
    caps
    delay-accounting [[ description = [ Displays delay accounting info from the Linux kernel ] ]]
    lm_sensors
    pcp [[ description = [ Build a pcp-htop binary for Performance Co-Pilot (PCP) support ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        backtrace? ( dev-libs/libunwind )
        caps? ( sys-libs/libcap )
        delay-accounting? ( net-libs/libnl:3.0 )
        lm_sensors? ( sys-apps/lm_sensors )
        pcp? ( monitor/pcp )
    suggestion:
        dev-util/strace  [[ description = [ Trace system calls of the selected process ] ]]
        sys-process/lsof [[ description = [ Display a list of open files of the selected process ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Help htop to find our ncurses library when cross-compiling
    # NOTE: this only works because ncursesw6-config is a bash script, ideally this would be done
    # using pkg-config, see https://github.com/hishamhm/htop/issues/671
    HTOP_NCURSESW_CONFIG_SCRIPT="/usr/$(exhost --target)/bin/ncursesw6-config"
    --enable-openvz
    --enable-unicode
    --enable-vserver
    --disable-ancient-vserver
    --disable-debug
    --disable-hwloc
    --disable-static
    --disable-werror
    --with-os-release=/etc/os-release
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'backtrace unwind'
    'caps capabilities'
    'delay-accounting delayacct'
    'lm_sensors sensors'
    pcp
)

